package com.vmo;

import java.util.List;

public class ServiceImpl implements Service {
    @Override
    public List<Student> readLines() {
        return null;
    }

    @Override
    public List<Student> calcAverageScore() {
        return null;
    }

    @Override
    public Student findHighestGPA(List<Student> studentList) {
        return null;
    }

    @Override
    public List<Student> findStudentHaveAverageScoreMoreThan5() {
        return null;
    }

    @Override
    public List<Student> sortAscStudentList(List<Student> studentList) {
        return null;
    }

    @Override
    public List<Student> findStudentListByCondition(List<Student> studentList, Student student) {
        return null;
    }
}
