package com.vmo;

import java.util.List;

// Tham khảo logic xử lý đọc ghi file https://stackoverflow.com/questions/66905788/read-text-file-line-by-line-and-store-names-into-list-in-java
public interface Service {
    // Tạo hàm đọc file và add dữ liệu trong file vào list Student
    List<Student> readLines();

    // Tạo hàm tính điểm trung bình cho từng người (Gợi ý: Tạo thêm 1 cột điểm trung bình trong Class student)
    List<Student> calcAverageScore();

    // Tạo hàm tìm người có điểm trung bình cao nhất
    Student findHighestGPA(List<Student> studentList);

    // Tìm tất cả những người có điểm trung bình >= 5
    List<Student> findStudentHaveAverageScoreMoreThan5();

    // Sắp xếp sinh viên theo điểm trung bình từ cao xuống thấp
    List<Student> sortAscStudentList(List<Student> studentList);

    // Tìm kiếm sinh viên theo từng điều kiện
    List<Student> findStudentListByCondition(List<Student> studentList, Student student);
}
